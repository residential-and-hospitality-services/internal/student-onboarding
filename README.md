Last Edit: July 15, 2019

Drupal 8 Quick Start Documentation

Contents {#contents .TOCHeading}
========

[Table of Contents 1](#_Toc14081564)

[Get Familiar With Terminal Command Line Interface
1](#get-familiar-with-terminal-command-line-interface)

[Setting Up a Development Environment
2](#setting-up-a-development-environment)

[Install Docker 3](#install-docker)

[Install Docker-Compose 3](#install-docker-compose)

[Install Devilbox -- a dockerized AMP stack
3](#install-devilbox-a-dockerized-amp-stack)

[Devilbox Shell 4](#devilbox-shell)

[Drush 4](#drush)

[MySQL 4](#mysql)

[Composer 5](#composer)

[Setting up a Drupal 8 Site With Devilbox
5](#setting-up-a-drupal-8-site-with-devilbox)

[Restoring a Drupal site from a tarball file (file.tgz or file.tar.gz)
6](#restoring-a-drupal-site-from-a-tarball-file-file.tgz-or-file.tar.gz)

[Using Devilbox 7](#using-devilbox)

[Without Devilbox 7](#without-devilbox)

[Converting Drupal Site to be Maintained by Composer
8](#converting-drupal-site-to-be-maintained-by-composer)

[Installing Drupal Modules 8](#installing-drupal-modules)

[Through Composer: 9](#through-composer)

[Through Drush: 9](#through-drush)

[Through the User Interface: 9](#through-the-user-interface)

[Removing Drupal Modules 10](#removing-drupal-modules)

[Updating Drupal Modules 10](#updating-drupal-modules)

[Updating Drupal Core Module: 11](#updating-drupal-core-module)

[Non-Core modules: 11](#non-core-modules)

[Getting Started Building Things in Drupal 8
12](#getting-started-building-things-in-drupal-8)

[Migrating Drupal 7 site to Drupal 8
12](#migrating-drupal-7-site-to-drupal-8)

[Bash Shell Aliases 13](#bash-shell-aliases)

[Git 14](#git)

[MSU VPN Setup 16](#msu-vpn-setup)

[Shared Network Drive Access (V Drive)
17](#shared-network-drive-access-v-drive)

 {#section-1 .ListParagraph}

Get Familiar With Terminal Command Line Interface
=================================================

Many of the tools that will be used to manage a Drupal project are
available through the Command line Interface (CLI). It will be helpful
to get comfortable using the terminal first.

Open up the terminal application and follow along with the tutorial at
[[Ryan's
Tutorials]{.underline}](https://ryanstutorials.net/linuxtutorial/commandline.php)

Quickly switch tutorial sections with the drop down menu at the top
right of the page.

Depending on your previous experience review any material in the
following sections that you are unfamiliar with:

1.  Command Line

2.  Basic Navigation

3.  More About Files

4.  Manual Pages

5.  File Manipulation

6.  VI -- Text Editor

7.  Permissions

The rest of the sections are great to know but less important when first
starting out. Especially:

1.  Grep and Regular Expressions

2.  Piping and Redirection

3.  Wildcards

<!-- -->

3.  

    Setting Up a Development Environment
    ====================================

Commands which should be run in the terminal are denoted by a line
starting with '\$'. Copy and paste those lines in your terminal
excluding the '\$ '

Install Docker
--------------

[[Documentation for installation on
Fedora]{.underline}](https://developer.fedoraproject.org/tools/docker/docker-installation.html)

dnf is the software which manages your Fedora software packages. It
leverages the software yum. If you see install guides in the future
referring to yum, they are essentially the same thing.

**Install docker software:**

\$ sudo dnf install docker

**Start docker daemon ( aka background process )**

\$ sudo systemctl start docker

**Test your installation**

\$ sudo docker run hello-world

**Start the docker daemon automatically during system startup**

\$ sudo systemctl enable docker

Install Docker-Compose
----------------------

\$ sudo dnf install docker-compose

Install Devilbox -- a dockerized AMP stack
------------------------------------------

Well not exactly, since devilbox installs Nginx instead of Apache. LAMP
is the most well known stack, so I wanted to mention the similarity.
Apache and Nginx both provide the software to run a web server. PHP is
the server side scripting language which just means it is the
programming language that runs on the server.

Download devilbox:\
\$ git clone https://github.com/cytopia/devilbox

Follow the rest of the [install
documentation](https://devilbox.readthedocs.io/en/latest/getting-started/install-the-devilbox.html)
to setup your .env file.

Note: SELinux is enabled by default for Fedora. This means that the .env
file that is created in part 2.2 will require the MOUNT\_OPTIONS
configuration change specified in part 2.4.1.

To start the Devilbox containers, run the command:

\$ docker-compose up

Add the -d flag in order to run the containers in the background.

The containers run the nginx, MySQL, and PHP software of the LAMP stack.

While the containers are running, you may go to localhost in your web
browser to see Devilbox status.

Devilbox Shell
--------------

The Devilbox shell allows the use of drush, PHP, and MySQL

To enter the shell, go to the devilbox install directory and enter:

\$ ./shell.sh

Note: I will sometimes refer to this shell as the PHP container.

This will drop you into a new shell in a location which mirrors the
devilbox/data/www/ directory. Cd into any of the site directories'
htdocs in order to run drush.

### Drush

Documentation of drush command [here](https://drushcommands.com/).

Drush allows you to do many site specific operations from the terminal.
A few examples are:

-   The ability to create a new account and give it admin rights.

-   Create a backup of the entire site. (files + database)

-   Update/install modules.

-   Clear JS and CSS cache to see the updates to frontend of the site
    when editing JS and CSS files.

I encourage you to explore the documentation above for more use cases.

### MySQL

Quick [cheatsheet for MySQL commands](https://devhints.io/mysql).

MySQL is the database driver that you will likely be using. Mariadb is a
fork of MySQL and functions very similarly.

### Composer

Composer is the PHP dependency manager used with Drupal.

Drupal's project is starting to move away from using drush to manage
packages/modules in favor of using composer. It will be important to get
comfortable with installing, removing, and updating modules using
composer as well as drush for that reason. Composer's documentation is a
good place to start understanding the hows and whys for composer.
Additionally: A quick [cheatsheet for using
composer](https://devhints.io/composer) as well as a [introductory blog
post](https://www.grazitti.com/blog/why-composer-is-the-best-practices-for-updating-drupal-8-core-and-modules/).

Setting up a Drupal 8 Site With Devilbox
========================================

Make sure you are in the devilbox/data/www directory before starting.

For this initial site setup, I will be calling it hellodrupal.

Create a directory named hellodrupal inside the www directory:

\$ mkdir \~/devilbox/data/www/hellodrupal

Then use cd to enter the new directory before continuing.

Download the most up to date Drupal 8 software:

\$ wget <https://www.drupal.org/download-latest/tar.gz>

extract files: \$ tar -xzf tar.gz

Use mv command to rename the new directory to htdocs. Devilbox requires
that site files are placed inside a directory named htdocs.

The directory structure should now look like
\~/devilbox/data/www/hellodrupal/htdocs

cd into the htdocs directory before continuing.

Create the files directory for Drupal: \$ mkdir sites/default/files

Give write permission to Drupal: \$ sudo chmod o+w sites/default/files

Add a virtual host entry to your hosts file:

\$ sudo vi /etc/hosts

Insert this line: 127.0.0.1 hellodrupal.loc

Now you can visit your new Drupal site in the browser. Navigate to
[[http://hellodrupal.loc]{.underline}](http://hellodrupal.loc/)

Follow the install prompts. When you get the database portion enter the
following information:

-   database type: mysql

-   database: hellodrupal

-   user: root

-   user-password: "" (empty)

    -   advanced:

        -   change server from localhost to 127.0.0.1 for devilbox to
            work properly

You should now have a fully functional Drupal site to play around with!

Restoring a Drupal site from a tarball file (file.tgz or file.tar.gz)
=====================================================================

Our Drupal sites are distributed via tarballs. It is an archive file
holding all of the project files as well as a SQL file to create a
database mirroring the original site.

Begin by extracting the files from the tarball with:

\$ tar -xzf tarball.tgz

-x tells the program to extract, -z means to filter it through GZip, and
-f allows you to point to the tarball file.

Continue restoring the site by following one of the following applicable
sections.

Using Devilbox
--------------

starting in the data/www directory with the site files extracted
already:

-   Assign the website a virtual host at /etc/hosts

    -   \$ sudo vi */*etc/hosts

    -   Enter something in the form of: 127.0.0.1 YOURSITE.loc

-   Create a database and reproduce the data with the provided SQL file:

    -   Note the path to your SQL file. Then enter the PHP container.

    -   Enter the MySQL Command Line Interface (CLI):

        -   \$ mysql -u root -p -h 127.0.0.1

    -   Now create a database: \$ create database
        \<your-site-database\>;

    -   Select the new database to perform operations on it with the
        'use' command:

        -   \$ use \<your-site-database\>;

    -   Now actually restore the database with the 'source' command:

        -   \$ source \<path-to-the-sqlfile\>;

-   Edit the settings.php file with the new database's information. If
    it doesn't exist, then copy the default.settings.php file from
    sites/default, and name the new file settings.php

    -   Explicitly: the user, password, database, and host settings

-   Verify that a files directory exists with write permission allowed
    to Drupal:

    -   directory should be at \<site-root\>/sites/default/files

    -   Assuming this is a development environment, just give write to
        other:

        -   \$ chmod o+w -R files

    2.  Without Devilbox
        ----------------

The general steps are to:

-   Assign the website a virtual host at /etc/hosts

    -   \$ sudo vi */etc/hosts*

    -   Enter something in the form of: 127.0.0.1 YOURSITE.loc

    -   Note: if not using devilbox, you will also have to configure
        Apache or its equivalent to serve your Drupal files when
        receiving a request from the browser for the domain you just
        entered. Otherwise you will not get a web response.

-   Create a database and reproduce the data with the provided SQL file

    -   Involves authenticating with MySQL, use-ing your selected
        database and running

    -   MySQL\> source DATABASE\_DUMP.sql

-   Edit the settings.php file with the new database's information. If
    it doesn't exist, then copy the default.settings.php file from
    sites/default, and name the new file settings.php

    -   Explicitly: the user, password, database, and host settings

-   Create a files directory and give write permission to Drupal (Can
    usually be done with www-data group)

    -   \$ mkdir YOURSITEROOT/sites/default/files

    -   \$ chgrp www-data -R sites/default/files

Converting Drupal Site to be Maintained by Composer
===================================================

If you have an existing Drupal site in which the site dependencies are
not being handled by composer (e.g. not using composer
require/update/remove to install, update, or uninstall modules) follow
these steps to convert it over to composer:

1.  If composerize-drupal has not been installed yet do so with:

> \$ composer global require grasmash/composerize-drupal\
> NOTE: You do not have to install composer. It is installed in devilbox
> by default. Access it inside the PHP shell.

2.  Now to composerize to project navigate to the project root directory
    and run:\
    \$ composer composerized-drupal \--composer-root=.
    \--drupal-root=\<dir-holding-drupal-files\...htdocs with devilbox\>

3.  If the command was successful, the project has been composerized.

Generally the composer-root will be */*shared*/httpd/\<site-dir\>*

Installing Drupal Modules
=========================

There are three main ways I will cover. The one you use will depend on
how the site is setup to manage its modules, and whether or not you have
direct access to the site.

This section assumes you are inside the site directory that you want to
install modules for.

Note: A particular module's machine name can be found from its
Drupal.org page. For example the module FullCalendar View has a page at
[[https://www.drupal.org/project/fullcalendar\_view]{.underline}](https://www.drupal.org/project/fullcalendar_view),
and its machine name is fullcalendar\_view.

### Through Composer:

By default Drupal uses composer to manage PHP dependencies for its Core
files. Composer maintains a list of installed modules and the version
that is installed. When requiring modules you may specify which versions
are allowed to be installed.

For example you could limit the dependency to one specific version, a
range of versions, or to the current major release while still allowing
updates minor releases. See: [[Version
Control]{.underline}](https://getcomposer.org/doc/articles/versions.md#writing-version-constraints).

If the site is setup to use composer to manage third-party modules
follow this procedure:

\$ composer require drupal/\<drupal-module-machine-name\>:"\<version\>"

That will set the module to be tracked and updated by composer. Now the
module must be enabled through Drush:

\$ drush8 en \<drupal-module-machine-name\>

\`

### Through Drush:

To install module files simply run: \$ drush8 dl
\<drupal-module-machine-name\>

Then to enable the use of the module run: \$drush8 en
\<drupal-module-machine-name\>

### Through the User Interface:

Requires that the update manager module is installed and enabled.
Navigating to 'extend' or /admin/modules will allow you to reach an
install form where you can enter the URL for the module that should be
installed.

This is generally a last resort option. Generally the proper way to get
modules installed on production sites which you do not have direct file
access to is to get an up to date Tarball from someone, then run through
either the composer or drush procedure to install the module. Then you
commit the file changes to git and push those changes to the remote git
server.

Removing Drupal Modules
=======================

Drush:

\$ drush8 pmu \<drupal-module-machine-name\>

To completely remove the module files:

\$ rm Rf \<path-to-module-files-dir\>

Composer:

\$ drush8 pmu \<drupal-module-machine-name\>

\$ composer remove drupal/\<drupal-module-machine-name\>

Alternatively - through the User Interface:

Admin Menu \> Extend \> Uninstall module tab \> Select desired module

\*Also will have to do composer step if project is using composer to
manage modules.

Updating Drupal Modules
=======================

Modules will periodically get updated to fix bugs and vulnerabilities.
It is important to keep everything as up to date as possible. Depending
on the site, the process is only slightly different depending on whether
non-core modules are managed by composer. To be safe, you may want to
backup the site with drush before updating in case something breaks
during the process. At the very least ensure you have the project update
to date in git.

First check to see which modules possibly including Core, have available
updates:

With composer: \$ composer outdated "drupal/\*"

With drush: \$ drush8 ups

(ups = update-status)

When updating more than one module make sure to update/install one
module at a time and verify that the site still works. If all is well,
commit the file changes to git with a commit message which states the
module that was updated and the version it was updated to. Repeat this
process for all modules.

4.  ### 

    ### 

    ### Updating Drupal Core Module:

\*\* Ensure you have a backup before proceeding. (\$ drush ard
--destination="\<insert-path-here\>")

Composer:

\$ composer update drupal/core \--with-dependencies

\$ drush8 updatedb

\$ drush8 cr all

To verify installed version: \$ drush8 st

Check that the site is still functional. If good, commit changes to git.

Drush:

\$ drush8 up drupal

\$ drush8 updatedb

\$ drush8 cr all

To verify installed version: \$ drush8 st

Check that the site is still functional. If good, commit changes to git.

### Non-Core modules:

Composer:

\$ composer update drupal/\<module-machine-name\> \--with-dependencies

Check that the site is still functional. If good, commit changes to git.

Drush:

\$ drush8 up \<module-machine-name\>

Check that the site is still functional. If good, commit changes to git.

Getting Started Building Things in Drupal 8
===========================================

The sections below will outline the most important aspects of the user
guide. They are important for understanding how Drupal structures its
data, and works in general. Getting somewhat familiar with this content
will setup the fundamentals required to know where and how to build
functionality.

Drupal.org: https://www.drupal.org/docs/user\_guide/en/index.html

-   Chapter 1 Sections: 1.1, 1.2, 1.3, and 1.5

-   Chapter 2 Sections: 2.1, 2.3, and 2.5

-   Chapter 4 Sections: 4.1, briefly 4.2, 4.5, and 4.6

-   Chapter 5 Sections: 5.1, and 5.6

-   Chapter 6 Sections: 6.1, 6.3, 6.5, 6.8, 6.10, 6.12, 6.15

-   Chapter 7 Section: 7.1

-   Chapter 8 Sections: 8.1, 8.2, 8.3

-   Chapter 9 Sections: 9.1, 9.2, 9.3, 9.5

Acquia Youtube Tutorials:

Some of these tutorials may overlap a bit with the Drupal.org pages I
listed above. Feel free to skip if you feel comfortable with the topic.

Entire General playlist:
[[https://www.youtube.com/playlist?list=PLpVC00PAQQxGwyvUD\_tYcBbLJqRC1CZ6U]{.underline}](https://www.youtube.com/playlist?list=PLpVC00PAQQxGwyvUD_tYcBbLJqRC1CZ6U)

Entire Themeing playlist:
[[https://www.youtube.com/playlist?list=PLpVC00PAQQxG0sW9YOueVgouRp4aj1bng]{.underline}](https://www.youtube.com/playlist?list=PLpVC00PAQQxG0sW9YOueVgouRp4aj1bng)

General playlist videos of interest:

-   Lessons: 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 22, 23, 26, 27, 28,
    30, 31, 33, 36\*, 38, 39, 40, 53

Themeing playlist videos of interest:

-   Lessons: 9, 11, 12, 13, 14, 21, 23, 24, 26, 27, 31, 32,

If doing primarily front-end work, you may want to watch the entire
playlist.

If primarily handling more site building tasks, focus on 9, 11, 21, 23,
24, 26, 27

Keys will be cache rebuilding, understanding what the theme really is,
and how to make small edits to twig files to get more control with
views.

For a guide on twig file naming conventions in order to override base
templates, see this page:

[[https://www.drupal.org/docs/8/theming/twig/twig-template-naming-conventions]{.underline}](https://www.drupal.org/docs/8/theming/twig/twig-template-naming-conventions)

Migrating Drupal 7 site to Drupal 8
===================================

Skip if not currently tasked with performing a migration.

Migrating Drupal sites is all about maintaining as much content as
possible as the site is transitioned to the newer version on Drupal.
Content, Files, and user accounts all persist following a successful
migration. Some content cannot be successfully migrated however, and
will have to be rebuilt. The following steps will outline the various
steps required to migrate the data, and then rebuild the site in Drupal
8.

The Drupal 7 site is called the source site as it is the source of data.
The site should be accessible locally.

The Drupal 8 site is the target site.

1.  Module Audit

    -   Gather the list of modules that are installed and used on the
        source site.

    -   For each module check to see if it has a corresponding Drupal 8
        version. Some will, and some will not. Be careful about using
        modules which only have dev or pre-release versions available as
        they may have security flaws or bugs. Sometimes modules are
        abandoned in favor of new ones with similar functionality.

    -   Note if the source site is even using a particular module still,
        if it is not you may be able to omit it from the migration
        process. Check with team to make sure it is no longer needed.

2.  Take Note of Existing content on the source site

    -   Content pages -- Will want to make sure the content is roughly
        intact after the migration. Make sure images make it through the
        process and are still linked to associated content.

    -   Views -- All views have to be rebuilt in Drupal 8.

    -   Webforms -- All webforms have to be rebuilt in Drupal 8.

    -   Note the general layout of the front page. Which views and
        blocks are included? 1 or 3 column?

3.  Begin setting up a target Drupal 8 site locally

    -   Download the latest release of Drupal 8 and go through normal
        setup until it is up and running.

    -   Enable the Drupal 8 migration modules: migrate, migrate\_drupal,
        and migrate\_drupal\_ui

    -   Install and enable all modules that have a corresponding 1:1
        match

    -   Install the libraries module, and place any module dependency
        libraries in the htdocs/libraries directory.

4.  On the target site, navigate to
    [[http://target-site.loc/upgrade]{.underline}](http://target-site.loc/upgrade)
    [and follow the prompts.]()

5.  Once finished, check out the content on the site to make sure
    content including images were successfully migrated. Make sure no
    critical information is missing before moving on.

6.  Install and configure the intended theme for the target site. This
    is usually the rhsbase theme.

    -   Add the Google Analytics code to the theme settings. The code
        can be obtained by inspecting the page source of the source
        site, and Ctrl-f searching for "GA" the code will be something
        like: UA-4754723-2

7.  Cleanup the block layout

    -   Some blocks will have migrated over in a non-functioning state.

    -   Disable or remove blocks that will not be used in the site.

    -   Place blocks in the correct theme regions.

8.  Go through every content type and configure each so they mirror the
    source site as closely as possible.

    -   Ensure content is added to the sitemap. If the site does not
        have a sitemap, it needs one. Pick up the simple\_sitemap module
        if needed.

    -   Uncheck 'promoted to the top'

    -   Check the form settings and make sure the proper widgets are
        selected for each field. This is important as content editors
        use the form to create content.

    -   Check the display settings and ensure they produce a page
        simliar to the source site. ( aside from differences in the
        theme )

    -   For image fields make sure to select the correct image style. (
        You may have to create new image styles in a later step and come
        back to this part )

9.  Rebuild all Views on the target site

10. Make sure to include webform library dependencies:

    -   With Composer:

        1.  If you don't have the Composer Merge Plugin required for the
            project go ahead and run:

            -   \$ composer require wikimedia/composer-merge-plugin

        2.  note the path from the composer.json file to the webform
            module install location. It will have a file named
            composer.libraries.json which defines the webform libraries
            we need.

        3.  Include the following snippet in the target site's
            composer.json under the "extra" key. If the snippet already
            exists with another path in the array, just add another one
            with the path you found in the previous step.

            \"merge-plugin\": {

\"include\": \[

\"web/modules/contrib/webform/composer.libraries.json\"

\]

},

Edit the value in the 'include' key to match the path found in previous
step.

4.  Run: \$ composer update \--lock

-   With Drush:

    5.  Simple enough just run: \$ drush8 webform-libraries-download

        -   However, do not use this if the rest of the project's
            dependencies are being maintained by composer.

11. Rebuild all webforms potentially including the contact form

12. Install, enable, and configure both of the antibot and honeypot
    modules.

    -   They mitigate the amount of spam our forms will receive.

13. Configure image styles and crop styles.

    -   At the time of writing this the best solution for Drupal 8 to
        allow content editors to crop images is the image\_widget\_crop
        module

    -   Module requires creating image styles and crop styles. The crop
        should be configured to manual crop and then scale width to the
        size of the images for that particular content type on the
        source site. Unless otherwise requested of course.

    -   Each content type with an image will require the crop configured
        in the form settings section for the content type after
        selecting to use imageWidgetCrop as the form widget.

14. Configure the main menu to mirror what existed for the source site.
    ( if the migration did not catch it )

15. Ensure pages exist for a contact info page, privacy policy page, and
    accessibility page.

    -   Rewrite the URL of those pages so that they line up with how you
        configured the theme.

    -   If done correctly the links in the footer will link to these
        pages.

16. Rebuild the front page using the panels and page\_manager modules

    -   When using both, you can select the layout which matches the
        front page of the source site.

    -   Then you can place normal blocks and Views blocks where they
        need to go.

    -   Set the URL to home

17. In general system settings make sure to set the front page to match
    the URL of the front page you just created. I.e /home

18. Ensure that context based blocks are configured correctly. For
    example 'helpful links' or 'quick links' blocks are only shown on
    particular pages. Base the behavior off of the source site.

19. Configure text editors

    -   Clear and invalid errors by going through all the editor types
        and saving them without changing anything.

    -   Site editors need access to Full HTML

    -   Make sure to drag buttons which would be useful as an editor
        onto the WYSIWYG editor. Try to keep them logically organized.

    -   Delete the PHP Code editor or remove all access. Direct PHP code
        is bad practice in Drupal 8

20. Navigate to Reports \> Error Log

    -   Make sure that there are no outstanding errors. Especially
        recurring errors.

21. Navigate to Reports \> Status Report

    -   Make sure there are no issues here as well. Trusted host
        settings can be ignored as they are set on the production
        server.

22. Run an accessibility audit on the new site.

    -   RHS websites must comply with acccessibility guidelines. If you
        have not run an accessibility audit yet, check with a coworker
        or Annette Burge for details.

23. Uninstall any development modules like migrate, migrate\_drupal,
    migrate\_drupal\_ui, and devel

24. Change user permissions so only Admins can create accounts

    -   Anonymous Users should only have view permissions to published
        content

25. Double check everything.

Bash Shell Aliases
==================

Bash Aliases are just short names for equivalent commands. Usually if
you find yourself writing the same long commands frequently, those are
the commands you would want to make into an alias.

Type 'alias' into your terminal in order to see the existing aliases by
default.

These aliases are user and shell dependent, so if you switch users, or
shell environments, the aliases will not transfer.

To add an alias in bash edit your bashrc file which is hidden by
default.

\$ nano \~/.bashrc

Place your custom aliases with the default aliases in the form:

alias \<alias\>="\<The command you want ran\>"

Ex: alias dbshell="cd \~/devilbox;./shell.sh"

bonus: alias www="cd \~/devilbox/data/www"

Your list of new aliases will be available when you either:

1\. open a new terminal session.

2\. run source \~/.bashrc

Git
===

If you have never worked with Git before, I recommend you go read
through the following sections of the [[git
handbook]{.underline}](https://git-scm.com/book/en/v2):

-   Part 1: 1.1, 1.3

-   Part 2: Entire section

-   Part 3: Entire section

-   Make sure to follow along so you can develop a work flow, and work
    through issues that you run into.

Once you have a grasp of those areas at some point look into how to
handle submodules. They are essentially embedded git repositories which
you track with a parent repository. We use submodules to manage our
site's themes as of the last recorded edit to this document.

Resources: [[Git
handbook]{.underline}](https://git-scm.com/book/en/v2/Git-Tools-Submodules),
and [[helpful blog
post]{.underline}](https://chrisjean.com/git-submodules-adding-using-removing-and-updating/)

Connecting to GitLab Server

Our code repositories are located at
[[https://issourcecontrol1.rhs.msu.edu]{.underline}](https://issourcecontrol1.rhs.msu.edu/)

To login, use your MSU NetID and RHSNET password.

However, in order to access that website, you must configure the MSU VPN
and have it active.

Setting up SSH keys

While you can get by using https protocol to pull and push code, it
requires you to enter your username and password each time. Consider
setting up SSH keys:

Key Generation:

\$ ssh-keygen -o -t rsa -b 4096 -C
\"[[email\@example.com]{.underline}](mailto:email@example.com)[\"]()

\$ ssh-add \~/.ssh/id\_rsa

Git Config change:

Add the following lines to your \~/.ssh/config file:

match canonical host \*.rhs.msu.edu

user winnerc2

match canonical host \*.rhs.msu.edu !host isjump0.rhs.msu.edu

ProxyJump isjump0

match all

CanonicalizeHostname yes

CanonicalDomains rhs.msu.edu

StrictHostKeyChecking no

ServerAliveInterval 20

ServerAliveCountMax 4

TCPKeepAlive no

ExitOnForwardFailure yes

HashKnownHosts no

UpdateHostKeys yes

LogLevel quiet

Gitlab Setup:

For this part you just need to login at
[[https://issourcecontrol1.rhs.msu.edu]{.underline}](https://issourcecontrol1.rhs.msu.edu/)
and click on your profile icon at the top right. Go to Settings. From
here go to SSH Keys which is on the left-hand sidebar. Now copy the
entire contents of your \~/.ssh/id\_rsa.pub file into the 'key' textbox.
Click 'Add key' and you are done.

You can now use ssh protocol to push/pull/clone repositories from
GitLab.

MSU VPN Setup
=============

![](media/image1.png){width="2.9631944444444445in"
height="3.5902777777777777in"}

Setup is for Fedora, but should be similar on other Linux distributions.

Go to settings by hitting your Super Key and typing settings. Navigate
to network on the left-hand sidebar. Click the cog icon next to the VPN
section.

Navigate to the identity tab, and change the VPN Protocol and Gateway as
shown on the right. Click apply when done.

![](media/image2.png){width="1.0173611111111112in" height="0.25in"}You
can now connect by clicking the on/off tab in settings. You can also
turn it on via clicking the menu in the top right

Shared Network Drive Access (V Drive)
=====================================

If you are on Windows then you have access to the V Drive by default.

-   If on a Linux system, run the script create-v-drive-stuff.sh with
    sudo priviledges.

    -   It will ask for your UNIX user (the name in the terminal)

    -   As well as your RHSNET user name which should be your NetID.

    -   The password you supply should be the password to RHSNET. The
        one you use to login to Windows.

-   Now you can mount and umount the V Drive with sudo mount and sudo
    umount respectively.

    -   The most common use case is to transfer big files like tarballs.

    -   Depending on the part of the V Drive you need to access, you
        would mount one of /net/\<dir\>

        -   You can see a list of available mounts with \$ ls /net

        -   For Example: \$ sudo mount /net/IS

    -   Now you can cd through /net/IS like a normal file system. Files
        are likely in:

        -   */*net/IS/Read-Write/SEAL\\ Team -Note the character escape
            in SEAL Team.

-   Remember to unmount the V Drive when you are done.

    -   \$ Sudo umount /net/IS

-   NOTE: If you ever change your RHSNET password, you will need to edit
    it:

    -   Sudo vi /etc/cifs.rhsnet.creds
