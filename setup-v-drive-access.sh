#!/usr/bin/env bash
#
# create-v-drive-stuff.sh (#) 190123/nepella
# Renee Margaret McConahy <nepella@gmail.com>
#<
# Usage: create-v-drive-stuff.sh
#
# Create the Linux equivalent of the "V:" drive.
#>

ROOT=
SHARES=(
    "apps"
    "Administrative Records"
    "ECenter Shared"
    "Financial Records"
    "Home"
    "IS"
    "legal"
    "Legal Records"
    "Media Outreach Files"
    "Meeting Files"
    "Personnel Records"
    "University Relation"
)
SERVER=isfileserv.rhsnet.rhs.msu.edu
CREDFILE=/etc/cifs.rhsnet.creds

fail() {
    [[ -n ${1-} ]] && echo "Error: $1" >&2
    exit 1
}

trap 'fail "Uncaught error at $(basename -- "$0"):$LINENO"' ERR
set -Euo pipefail

[[ "$(id -u)" == "0" ]] || fail "You need to run this as root."

read -ep "Local username: " -i "${SUDO_USER-}" username
read -ep "NetID: " -i "${SUDO_USER-}" netid
read -srp "Password: " password; echo

umask 0077

sed 's/^ \{4\}//' <<EOF >"$ROOT/$CREDFILE"
    username=$netid
    password=$password
    domain=RHSNET
EOF

mkdir -p -- "$ROOT/net"
chmod 0755 -- "$ROOT/net"

for share in "${SHARES[@]}"; do
    mkdir -p -- "$ROOT/net/$share"
    chmod 0 -- "$ROOT/net/$share"
    chattr +i "$ROOT/net/$share"

    escaped=${share// /\\040}

    echo "//$SERVER/$escaped /net/$escaped cifs vers=2.1,credentials=$CREDFILE,uid=$username,gid=$username,file_mode=0600,dir_mode=0700,ro,user,noauto,nofail,x-systemd.device-timeout=4" >>$ROOT/etc/fstab
done
